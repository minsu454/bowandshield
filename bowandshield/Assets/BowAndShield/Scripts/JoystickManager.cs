﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickManager : MonoBehaviour
{
    public Transform playerTr;
    public Rigidbody2D playerRb;
    public Transform[] joystickTr;

    private Vector2[] defaultJoypadScreenPos = new Vector2[2];
    private const float limitJoystickDistance = 80;
    private const float moveSpeed = 0.05f;

    public void MoveJoystickDown()
    {
        defaultJoypadScreenPos[0] = UICamera.mainCamera.WorldToScreenPoint(joystickTr[0].position);

        StartCoroutine("CoMoving");
    }

    public void MoveJoystickUp()
    {
        StopCoroutine("CoMoving");
        
        ResetJoypad(0);
    }

    public void DirJoystickDown()
    {
        defaultJoypadScreenPos[1] = UICamera.mainCamera.WorldToScreenPoint(joystickTr[1].position);

        StartCoroutine("CoDirection");
    }

    public void DirJoystickUp()
    {
        StopCoroutine("CoDirection");

        ResetJoypad(1);
    }

    public void ResetJoypad(int num) {
        joystickTr[num].localPosition = Vector2.zero;
        playerRb.velocity = Vector2.zero;
    }

    IEnumerator CoMoving() {
        while (true) {
            Vector2 touchScreenPos = UICamera.lastEventPosition;

            float dis = Vector2.Distance(defaultJoypadScreenPos[0], touchScreenPos);

            if (dis > limitJoystickDistance)
            {
                float angle = GetAngle(defaultJoypadScreenPos[0], touchScreenPos);
                Vector2 limitPos = GetLimitPos(defaultJoypadScreenPos[0], angle, limitJoystickDistance);
                joystickTr[0].position = UICamera.mainCamera.ScreenToWorldPoint(limitPos);
            }
            else {
                Vector2 touchWorldPos = UICamera.mainCamera.ScreenToWorldPoint(touchScreenPos);
                joystickTr[0].position = touchWorldPos;
            }

            //Move
            float disX = joystickTr[0].localPosition.x;
            float disY = joystickTr[0].localPosition.y;

            if (disX > 10 || disX < -10)
            {
                disX *= moveSpeed;
            }
            else {
                disX = 0;
            }

            if (disY > 10 || disY < -10)
            {
                disY *= moveSpeed;
            }
            else {
                disY = 0;
            }

            playerRb.velocity = new Vector2(disX, disY);

            yield return null;
        }
    }

    IEnumerator CoDirection() {
        while (true)
        {
            Vector2 touchScreenPos = UICamera.lastEventPosition;

            float dis = Vector2.Distance(defaultJoypadScreenPos[1], touchScreenPos);
            float angle = GetAngle(defaultJoypadScreenPos[1], touchScreenPos);
            float angle360 = GetAngle360(defaultJoypadScreenPos[1], touchScreenPos);

            if (dis > limitJoystickDistance)
            {
                Vector2 limitPos = GetLimitPos(defaultJoypadScreenPos[1], angle, limitJoystickDistance);
                joystickTr[1].position = UICamera.mainCamera.ScreenToWorldPoint(limitPos);
            }
            else
            {
                Vector2 touchWorldPos = UICamera.mainCamera.ScreenToWorldPoint(touchScreenPos);
                joystickTr[1].position = touchWorldPos;
            }

            playerTr.rotation = Quaternion.Euler(new Vector3(0, 0, -angle360));
            //playerTr.Rotate(new Vector3(0, 0, angle));

            yield return null;
        }
    }

    private float GetAngle(Vector2 startPos, Vector2 targetPos) {
        float disX = targetPos.x - startPos.x;
        float disY = targetPos.y - startPos.y;
        return Mathf.Atan2(disY, disX);
    }

    private float GetAngle360(Vector2 startPos, Vector2 targetPos)
    {
        float disX = targetPos.x - startPos.x;
        float disY = targetPos.y - startPos.y;
        return Mathf.Atan2(disX, disY) * 180f / Mathf.PI;
    }

    private Vector2 GetLimitPos(Vector2 startPos, float angle, float distance) {
        Vector2 returnVec = Vector2.zero;

        returnVec.x = distance * Mathf.Cos(angle) + startPos.x;
        returnVec.y = distance * Mathf.Sin(angle) + startPos.y;

        return returnVec;
    }
}
